﻿using CrmStudentApi.Models;
using CrmStudentApi.Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CrmStudentApi.Controllers
{
    public class HomeController : ApiController
    {
        private CrmStudentEntities crm = new CrmStudentEntities();

        // GET: api/Home
        public IHttpActionResult PostList([FromUri] HomeModel ho)
        {
            System.Guid userid = new Guid(ho.userid);
            var urlist = crm.T_Users_Roles.Where(p => p.uid == userid).ToList();
            List<T_Roles_Module> ro = new List<T_Roles_Module>();
            List<T_Module> mo = new List<T_Module>();
            foreach (var item in urlist)
            {
                //获取角色和模块的关联id
                ro = crm.T_Roles_Module.Where(p => p.rId == item.rId).ToList();
            }
            List<T_Module> data1 = new List<T_Module>();
            List<T_Module> data2 = new List<T_Module>();
            foreach (var roitem in ro)
            {
                var mo1 = crm.T_Module.FirstOrDefault(p => p.mId == roitem.mId);
                if (mo1.mfatherId == null)
                {
                    data1.Add(mo1);
                }
                else
                {
                    data2.Add(mo1);
                }
            }
            var model1 = data1.Select(p => new
            {
                mid = p.mId,
                mname = p.mname,
                murl = p.murl,
                mfatherId = p.mfatherId,
                mweight = p.mweight,
            });
            model1 = model1.OrderBy(p => p.mweight).ToList();
            var model2 = data2.Select(p => new
            {
                mid = p.mId,
                mname = p.mname,
                murl = p.murl,
                mfatherId = p.mfatherId,
                mweight = p.mweight,
            });
            model2 = model2.OrderBy(p => p.mweight).ToList();
            return Json(new { code = 0, data1 = model1, data2 = model2 });
        }

        /// <summary>
        /// 签到签退
        /// </summary>
        /// <param name="signin"></param>
        /// <returns></returns>
        public IHttpActionResult Signin([FromBody] T_Signin signin)
        {
            DateTime start = DateTime.Today;
            DateTime End = DateTime.Today.AddDays(1);
            var sa = crm.T_Signin.Where(p => p.uId == signin.uId && p.signinTime >= start && p.signinTime <= End).FirstOrDefault();
            if (sa == null)
            {
                var ss = crm.T_Users.SingleOrDefault(p => p.uid == signin.uId);
                signin.sId = Guid.NewGuid();
                signin.uname = ss.uname;
                signin.signinTime = DateTime.Now;
                signin.state = "已签到";
                crm.T_Signin.Add(signin);
                var zhi = crm.SaveChanges();
                if (zhi > 0)
                {
                    return Json(new { code = true, res = "签到成功" });
                }
                return Json(new { code = false, res = "签到失败" });
            }
            else
            {
                if (sa.state == "已签到")
                {
                    sa.signoutTime = DateTime.Now;
                    sa.state = "已签退";
                    crm.Entry(sa).State = System.Data.Entity.EntityState.Modified;
                    var zhi = crm.SaveChanges();
                    if (zhi > 0)
                    {
                        return Json(new { code = true, res = "签退成功" });
                    }
                    return Json(new { code = false, res = "签退失败" });
                }
                else
                {
                    return Json(new { code = false, res = "今天已经签退了" });
                }
            }
        }

        /// <summary>
        /// 提醒签到签退
        /// </summary>
        /// <param name="signin"></param>
        /// <returns></returns>
        public IHttpActionResult AlertSignin([FromBody] T_Signin signin)
        {
            DateTime start = DateTime.Today;
            DateTime End = DateTime.Today.AddDays(1);
            var sa = crm.T_Signin.Where(p => p.uId == signin.uId && p.signinTime >= start && p.signinTime <= End).FirstOrDefault();
            if (sa == null)
            {
                return Json(new { code = false, res = "今天还没有签到" });
            }
            else
            {
                if (sa.state == "已签到")
                {
                    return Json(new { code = false, res = "今天还没有签退" });
                }
                else
                {
                    return Json(new { code = true, res = "今天已经完成签到签退了" });
                }
            }
        }
    }
}