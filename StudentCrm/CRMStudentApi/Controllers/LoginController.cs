﻿using CrmStudentApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;
using System.Web.Security;

namespace CrmStudentApi.Controllers
{
    public class LoginController : ApiController
    {
        private CrmStudentEntities crm = new CrmStudentEntities();

        // GET: api/Login
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="us"></param>
        /// <returns></returns>
        public IHttpActionResult Login([FromBody] T_Users us)
        {
            var pwd = MD5(us.upwd);
            //用户名登录
            var user = crm.T_Users.SingleOrDefault(p => p.uname == us.uname && p.upwd == pwd);
            if (user == null)
            {
                //身份证登录
                user = crm.T_Users.SingleOrDefault(p => p.uidcard == us.uname && p.upwd == pwd);
            }
            if (user == null)
            {
                //手机号登录
                user = crm.T_Users.SingleOrDefault(p => p.uiPhone == us.uname && p.upwd == pwd);
            }
            if (user != null)
            {
                if (user.pwderror < 3)
                {
                    T_Users sa = crm.T_Users.Where(p => p.uid == user.uid).FirstOrDefault();
                    sa.pwderror = 0;
                    sa.finallytime = DateTime.Now;
                    sa.lockTime = null;
                    crm.Entry(sa).State = System.Data.Entity.EntityState.Modified;
                    crm.SaveChanges();
                    return Json(new { User = sa.uid, Success = true, Message = "登陆成功" });
                }
                else
                {
                    return Json(new { Success = false, Message = "账号已经上锁" });
                }
            }
            else
            {
                //用户名查询
                var users = crm.T_Users.SingleOrDefault(p => p.uname == us.uname);
                if (users == null)
                {
                    //身份证查询
                    users = crm.T_Users.SingleOrDefault(p => p.uidcard == us.uname);
                }
                if (users == null)
                {
                    //手机号查询
                    users = crm.T_Users.SingleOrDefault(p => p.uiPhone == us.uname);
                }
                if (users != null)
                {
                    if (users.pwderror < 3)
                    {
                        T_Users sa = crm.T_Users.Where(p => p.uid == users.uid).FirstOrDefault();
                        sa.pwderror = sa.pwderror + 1;
                        if (sa.pwderror >= 3)
                        {
                            sa.@lock = "是";
                            sa.lockTime = DateTime.Now;
                        }
                        crm.Entry(sa).State = System.Data.Entity.EntityState.Modified;
                        crm.SaveChanges();
                        return Json(new { Success = false, Message = "账号或者密码错误" });
                    }
                    else
                    {
                        return Json(new { Success = false, Message = "账号已上锁,联系管理员" });
                    }
                }
                return Json(new { Success = false, Message = "账号或者密码错误" });
            }
        }

        /// <summary>
        /// Md5转换
        /// </summary>
        /// <param name="inputStr"></param>
        /// <returns></returns>
        public static string MD5(string inputStr)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] hashByte = md5.ComputeHash(Encoding.UTF8.GetBytes(inputStr));
            var sb = new StringBuilder();
            foreach (byte item in hashByte)
                sb.Append(item.ToString("x").PadLeft(2, '0'));
            return sb.ToString();
        }
    }
}