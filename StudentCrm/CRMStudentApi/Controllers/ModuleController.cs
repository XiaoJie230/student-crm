﻿using CrmStudentApi.Models;
using CrmStudentApi.Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace crms.Controllers
{
    public class ModuleController : ApiController
    {
        // GET: api/Module
        private CrmStudentEntities crm = new CrmStudentEntities();

        [HttpPost]
        /// <summary>
        /// 获取树形结构模块内容
        /// </summary>
        public IHttpActionResult GetTree()
        {
            var Modulelist = crm.T_Module.Where(p => true);
            var data = from p in Modulelist
                       where p.mfatherId == null
                       select new
                       {
                           Moduleid = p.mId,
                           Modulename = p.mname,
                           Path = p.murl,
                           Weight = p.mweight,
                           Parentid = p.mfatherId,
                           children = from t in Modulelist
                                      where t.mfatherId == p.mId
                                      select new
                                      {
                                          Moduleid = t.mId,
                                          Modulename = t.mname,
                                          Path = t.murl,
                                          Weight = t.mweight,
                                          Parentid = t.mfatherId,
                                      }
                       };
            return Json(new { data = data.ToList() });
        }

        /// <summary>
        /// 修改模块
        /// </summary>
        public IHttpActionResult EditMoudule([FromBody] ModuleModel moduleList)
        {
            var Modulelist = crm.T_Module.FirstOrDefault(x => x.mId == moduleList.Moduleid);
            Modulelist.mname = moduleList.Modulename;
            Modulelist.mfatherId = moduleList.Parentid;
            Modulelist.murl = moduleList.Path;
            Modulelist.mweight = moduleList.Weight;
            int n = crm.SaveChanges();
            if (n > 0)
            {
                return Json(new { success = true, message = "保存成功！" });
            }
            else
            {
                return Json(new { success = false, message = "保存失败！" });
            }
        }

        /// <summary>
        /// 新增模块
        /// </summary>
        public IHttpActionResult AddModule([FromBody] ModuleModel module)
        {
            var Modulename = crm.T_Module.FirstOrDefault(x => x.mname == module.Modulename);
            if (Modulename != null)
            {
                return Json(new { success = false, message = "模块已存在！" });
            }
            else
            {
                T_Module module1 = new T_Module();
                module1.mId = Guid.NewGuid();
                module1.mname = module.Modulename;
                module1.mfatherId = module.Parentid;
                module1.murl = module.Path;
                module1.mweight = module.Weight;
                var s = crm.T_Module.Add(module1);
                int n = crm.SaveChanges();
                if (n > 0)
                {
                    return Json(new { success = true, message = "新增模块成功！" });
                }
                else
                {
                    return Json(new { success = false, message = "新增模块失败！" });
                }
            }
        }

        [HttpPost]
        /// <summary>
        /// 删除模块
        /// </summary>
        public IHttpActionResult Delete([FromBody] ModuleModel module)
        {
            var list = crm.T_Roles_Module.SingleOrDefault(p => p.mId == module.Moduleid);
            if (list == null)
            {
                var Modulelist = crm.T_Module.FirstOrDefault(p => p.mfatherId == module.Moduleid);
                if (Modulelist == null)
                {
                    var li = crm.T_Module.SingleOrDefault(p => p.mId == module.Moduleid);
                    crm.Entry(li).State = System.Data.Entity.EntityState.Deleted;
                    var n = crm.SaveChanges();
                    if (n > 0)
                    {
                        return Json(new { success = true, message = "删除模块成功！" });
                    }
                    else
                    {
                        return Json(new { success = false, message = "删除模块失败！" });
                    }
                }
                else
                {
                    return Json(new { success = false, message = "该模块下有子模块，不能删除！" });
                }
            }
            else
            {
                return Json(new { success = false, message = "该模块正在被使用，不能删除！" });
            }
        }

        ///
        public IHttpActionResult Father()
        {
            var ss = crm.T_Module.Where(p => p.mfatherId == null).ToList();
            var list = from p in ss
                       select new
                       {
                           Moduleid = p.mId,
                           Modulename = p.mname,
                           Path = p.murl,
                           Weight = p.mweight,
                           Parentid = p.mfatherId,
                       };
            return Json(list);
        }
    }
}