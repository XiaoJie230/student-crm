﻿using CrmStudentApi.Models;
using CrmStudentApi.Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CrmStudentApi.Controllers
{
    public class NetStudentController : ApiController
    {
        private CrmStudentEntities crm = new CrmStudentEntities();

        // GET: api/Student
        /// <summary>
        /// 获取数据
        /// </summary>
        /// <param name="stu"></param>
        /// <returns></returns>
        public IHttpActionResult StudenList([FromBody] StudentModel stu)
        {
            //查找是否为经理
            var user = crm.T_Users_Roles.SingleOrDefault(p => p.uid == stu.wzid);
            System.Guid rid = new Guid("d315cd9b-d426-4046-b004-ef24e46b0c9a");
            var stulist = crm.T_Student.Where(p => true);
            if (user.rId != rid)
            {
                stulist = crm.T_Student.Where(p => p.Userwlid == stu.wzid && p.ext2 != "已删除");
            }
            else
            {
                stulist = crm.T_Student.Where(p => p.ext2 != "已删除");
            }

            if (!string.IsNullOrWhiteSpace(stu.Stuname))
            {
                stulist = stulist.Where(x => x.Stuname.Contains(stu.Stuname));
            }
            if (!string.IsNullOrWhiteSpace(stu.Phone))
            {
                stulist = stulist.Where(x => x.Phone == stu.Phone);
            }
            if (!string.IsNullOrWhiteSpace(stu.Education))
            {
                stulist = stulist.Where(x => x.Education == stu.Education);
            }
            if (stu.Fenpei == "是")
            {
                stulist = stulist.Where(x => x.Userzxname != null);
            }

            var count = stulist.Count();
            stulist = stulist.OrderBy(x => x.Stuid);
            stulist = stulist.Skip((stu.page - 1) * stu.limit).Take(stu.limit);
            List<T_Student> stuss = new List<T_Student>();
            foreach (var item in stulist)
            {
                stuss.Add(item);
            }
            stuss = stuss.ToList();
            var qq = from a in stuss
                     select new T_Student
                     {
                         Stuid = a.Stuid,
                         Stuname = a.Stuname,
                         Sex = a.Sex,
                         Age = a.Age,
                         Phone = a.Phone,
                         Url = a.Url,
                         Education = a.Education,
                         State = a.State,
                         Channel = a.Channel,
                         Website = a.Website,
                         keywords = a.keywords,
                         Attention = a.Attention,
                         Department = a.Department,
                         QQ = a.QQ,
                         WeChat = a.WeChat,
                         IsReport = a.IsReport,
                         Userwlid = a.Userwlid,
                         Userwlname = a.Userwlname,
                         Userzxid = a.Userzxid,
                         Userzxname = a.Userzxname,
                         Isvalid = a.Isvalid,
                         Validreason = a.Validreason,
                         IsReturnvisit = a.IsReturnvisit,
                         Firstvisittime = a.Firstvisittime,
                         IsDoor = a.IsDoor,
                         Doortime = a.Doortime,
                         Deposit = a.Deposit,
                         Deposittime = a.Deposittime,
                         IsPay = a.IsPay,
                         Paytime = a.Paytime,
                         Payamount = a.Payamount,
                         IsRefund = a.IsRefund,
                         RefundReason = a.RefundReason,
                         IsClass = a.IsClass,
                         Classremarks = a.Classremarks,
                     };
            return Json(new { count = count, data = qq });
        }

        /// <summary>
        /// 添加网络学生
        /// </summary>
        /// <param name="stu"></param>
        /// <returns></returns>
        public IHttpActionResult StudenAdd([FromBody] T_Student stu)
        {
            var user = crm.T_Users.SingleOrDefault(p => p.uid == stu.Userwlid);
            stu.Userwlname = user.uname;
            //先给网络咨询的id,分配权限的时候在修改
            stu.Userzxid = user.uid;
            stu.Stuid = Guid.NewGuid();
            crm.T_Student.Add(stu);
            var zhi = crm.SaveChanges();
            if (zhi > 0)
            {
                return Json(new { code = true, res = "添加学生成功" });
            }
            return Json(new { code = false, res = "添加学生失败" });
        }
        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public IHttpActionResult StudenEdit([FromBody] T_Student s)
        {
            T_Student sa = crm.T_Student.Where(p => p.Stuid == s.Stuid).FirstOrDefault();
            sa.Stuname = s.Stuname;
            sa.Sex = s.Sex;
            sa.Age = s.Age;
            sa.Phone = s.Phone;
            sa.Url = s.Url;
            sa.Education = s.Education;
            sa.State = s.State;
            sa.Channel = s.Channel;
            sa.QQ = s.QQ;
            sa.WeChat = s.WeChat;
            sa.IsReport = s.IsReport;
            crm.Entry(sa).State = System.Data.Entity.EntityState.Modified;
            var zhi = crm.SaveChanges();
            if (zhi > 0)
            {
                return Json(new { code = true, res = "修改成功" });
            }
            return Json(new { code = false, res = "修改失败" });
        }
        /// <summary>
        /// 假删除
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public IHttpActionResult StudenDel([FromBody] T_Student s)
        {
            T_Student sa = crm.T_Student.Where(p => p.Stuid == s.Stuid).FirstOrDefault();
            sa.ext2 = "已删除";
            crm.Entry(sa).State = System.Data.Entity.EntityState.Modified;
            var zhi = crm.SaveChanges();
            if (zhi > 0)
            {
                return Json(new { code = true, res = "删除成功" });
            }
            return Json(new { code = false, res = "删除失败" });
        }
        /// <summary>
        /// 获取咨询师分组
        /// </summary>
        /// <param name="roles"></param>
        /// <returns></returns>
        public IHttpActionResult ZXtype([FromBody] T_Users_Roles roles)
        {
            //fcdfb2d4-f12f-486d-a12a-649d237d2af4
            var stulist = crm.T_Users_Roles.Where(p => p.rId == roles.rId);
            List<T_Users> users = new List<T_Users>();
            foreach (var item in stulist)
            {
                var us = crm.T_Users.SingleOrDefault(p => p.uid == item.uid && p.udel != "已删除");
                users.Add(us);
            }
            var data = from item in users
                       select new T_Users
                       {
                           uid = item.uid,
                           uname = item.uname,
                           upwd = item.upwd,
                           uidcard = item.uidcard,
                           uiPhone = item.uiPhone,
                           uemail = item.uemail,
                           creatime = item.creatime,
                           finallytime = item.finallytime,
                           pwderror = item.pwderror,
                           lockTime = item.lockTime,
                           @lock = item.@lock,
                           udel = item.udel
                       };
            return Json(new { data = data });
        }
        /// <summary>
        /// 分配权限
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public IHttpActionResult StudenFen([FromBody] T_Student s)
        {
            if (s.Userzxid != null)
            {
                var user = crm.T_Users.SingleOrDefault(p => p.uid == s.Userzxid);
                T_Student sa = crm.T_Student.Where(p => p.Stuid == s.Stuid).FirstOrDefault();
                sa.Userzxid = s.Userzxid;
                sa.Userzxname = user.uname;
                crm.Entry(sa).State = System.Data.Entity.EntityState.Modified;
                var zhi = crm.SaveChanges();
                if (zhi > 0)
                {
                    return Json(new { code = true, res = "分配成功" });
                }
                return Json(new { code = false, res = "分配失败" });
            }
            return Json(new { code = false, res = "咨询师不能为空" });
        }
    }
}