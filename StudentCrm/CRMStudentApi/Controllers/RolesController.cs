﻿using CrmStudentApi.Models;
using CrmStudentApi.Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CrmStudentApi.Controllers
{
    public class RolesController : ApiController
    {
        private CrmStudentEntities crm = new CrmStudentEntities();

        // GET: api/Roles
        public IHttpActionResult PostRolesList([FromBody] RolesModel user)
        {
            var ss = crm.T_Roles.Where(p => true);

            var count = ss.Count();
            ss = ss.OrderBy(x => x.rId);
            ss = ss.Skip((user.page - 1) * user.limit).Take(user.limit);
            List<T_Roles> userss = new List<T_Roles>();
            foreach (var item in ss)
            {
                userss.Add(item);
            }
            var qq = from item in userss
                     select new T_Roles
                     {
                         rId = item.rId,
                         rname = item.rname
                     };
            return Json(new { count = count, data = qq });
        }

        public IHttpActionResult PostAddRoles([FromBody] T_Roles role)
        {
            role.rId = Guid.NewGuid();
            var ss = crm.T_Roles.Where(p => p.rname == role.rname).ToList();
            if (ss.Count() == 0)
            {
                if (role.rname != "")
                {
                    crm.T_Roles.Add(role);
                    var zhi = crm.SaveChanges();
                    if (zhi > 0)
                    {
                        return Json(new { code = true, res = "添加角色成功" });
                    }
                    return Json(new { code = false, res = "添加角色失败" });
                }
                return Json(new { code = false, res = "添加角色不能为空" });
            }
            return Json(new { code = false, res = "已经有此角色了" });
        }

        public IHttpActionResult PostDelRolese([FromBody] T_Roles role)
        {
            var URlist = crm.T_Users_Roles.FirstOrDefault(p => p.rId == role.rId);
            if (URlist == null)
            {
                var RMlist = crm.T_Roles_Module.FirstOrDefault(p => p.rId == role.rId);
                if (RMlist == null)
                {
                    crm.Entry<T_Roles>(role).State = System.Data.Entity.EntityState.Deleted;
                    var n = crm.SaveChanges();
                    if (n > 0)
                    {
                        return Json(new { success = true, message = "删除角色成功！" });
                    }
                    else
                    {
                        return Json(new { success = false, message = "删除角色失败！" });
                    }
                }
                else
                {
                    return Json(new { success = false, message = "该角色正拥有模块管理！" });
                }
            }
            else
            {
                return Json(new { success = false, message = "该角色正在被使用！" });
            }
        }

        public IHttpActionResult PostEditRoles([FromBody] T_Roles role)
        {
            var ss = crm.T_Roles.Where(p => p.rname == role.rname);
            if (ss != null)
            {
                if (role.rname != "")
                {
                    T_Roles sa = crm.T_Roles.Where(p => p.rId == role.rId).FirstOrDefault();
                    sa.rname = role.rname;
                    crm.Entry(sa).State = System.Data.Entity.EntityState.Modified;
                    var zhi = crm.SaveChanges();
                    if (zhi > 0)
                    {
                        return Json(new { code = true, res = "修改角色成功" });
                    }
                    return Json(new { code = false, res = "修改角色失败" });
                }
                return Json(new { code = false, res = "修改角色不能为空" });
            }
            return Json(new { code = false, res = "已经有此角色,不能重复角色名称" });
        }

        [HttpPost]
        [HttpGet]
        public IHttpActionResult GetAllModules()
        {
            var datalist = crm.T_Module.Where(p => p.mfatherId == null).ToList();
            var bb = datalist.Select(p => new
            {
                mId = p.mId,
                mname = p.mname,
                mfatherId = p.mfatherId,
                murl = p.murl,
                mweight = p.mweight
            }).ToList();
            List<Moduless> data = new List<Moduless>();
            foreach (var item in bb)
            {
                var list = crm.T_Module.Where(p => p.mfatherId == item.mId).ToList();
                var cc = list.Select(p => new Moduless
                {
                    Id = p.mId,
                    Name = p.mname,
                    ParentId = p.mfatherId,
                    Path = p.murl,
                    Weight = p.mweight
                }).ToList();
                var aa = new Moduless()
                {
                    Id = item.mId,
                    Name = item.mname,
                    ParentId = item.mfatherId,
                    Path = item.murl,
                    Weight = item.mweight,
                    children = cc
                };
                data.Add(aa);
            };
            return Json(new { data = data, code = 0 });
        }

        [HttpGet]
        [HttpPost]
        public IHttpActionResult EditRM([FromBody] EditRM rM)
        {
            int n = 0;
            System.Guid rid = new Guid("00000000-0000-0000-0000-000000000000");
            if (rM.Id != null)
            {
                rid = new Guid(rM.Id);
            }
            else
            {
                rid = new Guid("00000000-0000-0000-0000-000000000000");
            }
            if (rM.Value != null)
            {
                string[] strArray = rM.Value.Split(','); //字符串转数组
                List<Guid> abc = new List<Guid>();
                foreach (var g in strArray)
                {
                    if (g != "")
                    {
                        abc.Add(new Guid(g));
                    }
                    else
                    {
                    }
                }
                var datalist = crm.T_Roles_Module.Where(p => p.rId == rid).ToList();
                foreach (var item in datalist)
                {
                    crm.T_Roles_Module.Remove(item);
                    n += crm.SaveChanges();
                }
                foreach (var i in abc)
                {
                    var dd = new T_Roles_Module()
                    {
                        rmId = Guid.NewGuid(),
                        rId = rid,
                        mId = i
                    };
                    crm.T_Roles_Module.Add(dd);
                    n += crm.SaveChanges();
                }
            }
            else
            {
                var datalist = crm.T_Roles_Module.Where(p => p.rId == rid).ToList();
                foreach (var item in datalist)
                {
                    crm.T_Roles_Module.Remove(item);
                    n += crm.SaveChanges();
                }
            }
            if (n >= 0)
            {
                return Json(new { code = 0, msg = "修改成功！" });
            }
            else
            {
                return Json(new { code = -1, msg = "修改失败！" });
            }
        }

        [HttpPost]
        [HttpGet]
        public IHttpActionResult GetMyModules([FromBody] T_Roles role)
        {
            var datalist = crm.T_Roles_Module.Where(p => p.rId == role.rId).ToList();
            var list = new List<Guid>();
            foreach (var item in datalist)
            {
                list.Add(item.mId);
            }
            return Json(new { data = list, code = 0 });
        }
    }
}