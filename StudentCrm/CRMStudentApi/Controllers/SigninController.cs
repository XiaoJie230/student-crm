﻿using CrmStudentApi.Models;
using CrmStudentApi.Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CrmStudentApi.Controllers
{
    public class SigninController : ApiController
    {
        private CrmStudentEntities crm = new CrmStudentEntities();

        // GET: api/Signin
        public IHttpActionResult PostList([FromBody] SigninModel ho)
        {
            var list = crm.T_Signin.Where(p => true);
            if (!string.IsNullOrWhiteSpace(ho.name))
            {
                list = crm.T_Signin.Where(p => p.uname.Contains(ho.name));
            }
            var a = Convert.ToDateTime("0001/1/1 0:00:00");
            if (ho.sigininTime != a)
            {
                list = crm.T_Signin.Where(p => p.signinTime >= ho.sigininTime);
            }
            if (ho.siginoutTime != a)
            {
                list = crm.T_Signin.Where(p => p.signoutTime <= ho.siginoutTime);
            }

            var count = list.Count();
            list = list.OrderBy(x => x.signinTime);
            list = list.Skip((ho.page - 1) * ho.limit).Take(ho.limit);
            List<T_Signin> ss = new List<T_Signin>();
            foreach (var item in list)
            {
                ss.Add(item);
            }
            ss = list.ToList();
            var qq = from item in ss
                     select new T_Signin
                     {
                         sId = item.sId,
                         uId = item.uId,
                         uname = item.uname,
                         signinTime = item.signinTime,
                         signoutTime = item.signoutTime,
                         state = item.state
                     };
            return Json(new { count = count, data = qq });
        }
    }
}