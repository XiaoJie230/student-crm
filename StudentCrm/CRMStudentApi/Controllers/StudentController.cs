﻿using CrmStudentApi.Models;
using CrmStudentApi.Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CrmStudentApi.Controllers
{
    public class StudentController : ApiController
    {
        private CrmStudentEntities crm = new CrmStudentEntities();

        // GET: api/Student
        public IHttpActionResult StudenList([FromBody] StudentModel stu)
        {
            var stulist = crm.T_Student.Where(p => p.Userzxid == stu.wzid && p.ext2 != "已删除");
            if (!string.IsNullOrWhiteSpace(stu.Stuname))
            {
                stulist = stulist.Where(x => x.Stuname.Contains(stu.Stuname));
            }
            if (!string.IsNullOrWhiteSpace(stu.Phone))
            {
                stulist = stulist.Where(x => x.Phone == stu.Phone);
            }
            if (!string.IsNullOrWhiteSpace(stu.Education))
            {
                stulist = stulist.Where(x => x.Education == stu.Education);
            }
            var count = stulist.Count();
            stulist = stulist.OrderBy(x => x.Stuid);
            stulist = stulist.Skip((stu.page - 1) * stu.limit).Take(stu.limit);
            List<T_Student> stuss = new List<T_Student>();
            foreach (var item in stulist)
            {
                stuss.Add(item);
            }
            stuss = stuss.ToList();
            var qq = from a in stuss
                     select new T_Student
                     {
                         Stuid = a.Stuid,
                         Stuname = a.Stuname,
                         Sex = a.Sex,
                         Age = a.Age,
                         Phone = a.Phone,
                         Url = a.Url,
                         Education = a.Education,
                         State = a.State,
                         Channel = a.Channel,
                         Website = a.Website,
                         keywords = a.keywords,
                         Attention = a.Attention,
                         Department = a.Department,
                         QQ = a.QQ,
                         WeChat = a.WeChat,
                         IsReport = a.IsReport,
                         Userwlid = a.Userwlid,
                         Userwlname = a.Userwlname,
                         Userzxid = a.Userzxid,
                         Userzxname = a.Userzxname,
                         Isvalid = a.Isvalid,
                         Validreason = a.Validreason,
                         IsReturnvisit = a.IsReturnvisit,
                         Firstvisittime = a.Firstvisittime,
                         IsDoor = a.IsDoor,
                         Doortime = a.Doortime,
                         Deposit = a.Deposit,
                         Deposittime = a.Deposittime,
                         IsPay = a.IsPay,
                         Paytime = a.Paytime,
                         Payamount = a.Payamount,
                         IsRefund = a.IsRefund,
                         RefundReason = a.RefundReason,
                         IsClass = a.IsClass,
                         Classremarks = a.Classremarks,
                     };
            return Json(new { count = count, data = qq });
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public IHttpActionResult StudenEdit([FromBody] T_Student s)
        {
            T_Student sa = crm.T_Student.Where(p => p.Stuid == s.Stuid).FirstOrDefault();
            sa.Stuname = s.Stuname;
            sa.Sex = s.Sex;
            sa.Age = s.Age;
            sa.Phone = s.Phone;
            sa.Url = s.Url;
            sa.Education = s.Education;
            sa.State = s.State;
            sa.Channel = s.Channel;
            sa.QQ = s.QQ;
            sa.WeChat = s.WeChat;
            sa.IsReport = s.IsReport;
            sa.Userwlname = s.Userwlname;
            sa.Isvalid = s.Isvalid;
            sa.Validreason = s.Validreason;
            sa.IsReturnvisit = s.IsReturnvisit;
            sa.Firstvisittime = s.Firstvisittime;
            sa.IsDoor = s.IsDoor;
            sa.Doortime = s.Doortime;
            sa.Deposit = s.Deposit;
            sa.IsPay = s.IsPay;
            sa.Paytime = s.Paytime;
            sa.Payamount = s.Payamount;
            sa.IsRefund = s.IsRefund;
            sa.RefundReason = s.RefundReason;
            sa.IsClass = s.IsClass;
            sa.Classtime = s.Classtime;
            sa.Classremarks = s.Classremarks;
            sa.Consultantremarks = s.Consultantremarks;

            crm.Entry(sa).State = System.Data.Entity.EntityState.Modified;
            var zhi = crm.SaveChanges();
            if (zhi > 0)
            {
                return Json(new { code = true, res = "修改成功" });
            }
            return Json(new { code = false, res = "修改失败" });
        }
        /// <summary>
        /// 假删除
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public IHttpActionResult StudenDel([FromBody] T_Student s)
        {
            T_Student sa = crm.T_Student.Where(p => p.Stuid == s.Stuid).FirstOrDefault();
            sa.ext2 = "已删除";
            crm.Entry(sa).State = System.Data.Entity.EntityState.Modified;
            var zhi = crm.SaveChanges();
            if (zhi > 0)
            {
                return Json(new { code = true, res = "删除成功" });
            }
            return Json(new { code = false, res = "删除失败" });
        }
    }
}