﻿using CrmStudentApi.Models;
using CrmStudentApi.Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;

namespace CrmStudentApi.Controllers
{
    public class UsersController : ApiController
    {
        private CrmStudentEntities crm = new CrmStudentEntities();

        // GET: api/User
        public IHttpActionResult PostUsersList([FromBody] UserModel user)
        {
            var ss = crm.T_Users.Where(p => p.udel == "未删除");

            if (!string.IsNullOrWhiteSpace(user.uname))
            {
                ss = ss.Where(x => x.uname.Contains(user.uname));
            }
            if (!string.IsNullOrWhiteSpace(user.@lock))
            {
                ss = ss.Where(x => x.@lock == user.@lock);
            }

            var count = ss.Count();
            ss = ss.OrderBy(x => x.uid);
            ss = ss.Skip((user.page - 1) * user.limit).Take(user.limit);
            List<T_Users> userss = new List<T_Users>();
            foreach (var item in ss)
            {
                userss.Add(item);
            }
            userss = userss.ToList();
            var qq = from item in userss
                     select new T_Users
                     {
                         uid = item.uid,
                         uname = item.uname,
                         upwd = item.upwd,
                         uidcard = item.uidcard,
                         uiPhone = item.uiPhone,
                         uemail = item.uemail,
                         creatime = item.creatime,
                         finallytime = item.finallytime,
                         pwderror = item.pwderror,
                         lockTime = item.lockTime,
                         @lock = item.@lock,
                         udel = item.udel
                     };
            return Json(new { count = count, data = qq });
        }

        /// <summary>
        /// 修改锁定状态
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult PostEditIsLocked([FromBody] T_Users user)
        {
            var ss = crm.T_Users.Where(p => p.uid == user.uid);

            T_Users sa = crm.T_Users.Where(p => p.uid == user.uid).FirstOrDefault();

            if (sa.@lock == "是")
            {
                sa.@lock = "否";
                sa.lockTime = null;
                sa.pwderror = 0;
                crm.Entry(sa).State = System.Data.Entity.EntityState.Modified;
                var zhi = crm.SaveChanges();
                if (zhi > 0)
                {
                    return Json(new { code = true, res = "解锁成功" });
                }
                return Json(new { code = false, res = "解锁失败" });
            }
            else
            {
                sa.@lock = "是";
                sa.lockTime = DateTime.Now;
                crm.Entry(sa).State = System.Data.Entity.EntityState.Modified;
                var zhi = crm.SaveChanges();
                if (zhi > 0)
                {
                    return Json(new { code = true, res = "上锁成功" });
                }
                return Json(new { code = false, res = "上锁失败" });
            }
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public IHttpActionResult PostEditDel([FromBody] T_Users user)
        {
            T_Users sa = crm.T_Users.Where(p => p.uid == user.uid).FirstOrDefault();
            sa.udel = "已删除";
            crm.Entry(sa).State = System.Data.Entity.EntityState.Modified;
            var zhi = crm.SaveChanges();
            if (zhi > 0)
            {
                return Json(new { code = true, res = "删除成功" });
            }
            return Json(new { code = false, res = "删除失败" });
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public IHttpActionResult PostEdit([FromBody] T_Users user)
        {
            var ss = crm.T_Users.Where(p => p.uid == user.uid);

            T_Users sa = crm.T_Users.Where(p => p.uid == user.uid).FirstOrDefault();

            if (user.uname == "") { return Json(new { code = false, res = "用户名不能为空" }); }
            if (user.uiPhone == "") { return Json(new { code = false, res = "用户名不能为空" }); }
            if (user.uidcard == "") { return Json(new { code = false, res = "用户名不能为空" }); }
            if (user.uemail == "") { return Json(new { code = false, res = "用户名不能为空" }); }
            sa.uname = user.uname;
            sa.uiPhone = user.uiPhone;
            sa.uidcard = user.uidcard;
            sa.uemail = user.uemail;

            crm.Entry(sa).State = System.Data.Entity.EntityState.Modified;
            var zhi = crm.SaveChanges();
            if (zhi > 0)
            {
                return Json(new { code = true, res = "修改成功" });
            }
            return Json(new { code = false, res = "修改失败" });
        }

        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public IHttpActionResult PostEditpwd([FromBody] T_Users user)
        {
            T_Users sa = crm.T_Users.Where(p => p.uid == user.uid).FirstOrDefault();
            var pwd = MD5("123456");
            sa.upwd = pwd;
            crm.Entry(sa).State = System.Data.Entity.EntityState.Modified;
            var zhi = crm.SaveChanges();
            if (zhi > 0)
            {
                return Json(new { code = true, res = "重置成功" });
            }
            return Json(new { code = false, res = "重置失败" });
        }

        /// <summary>
        /// 获取权限分组
        /// </summary>
        /// <returns></returns>
        public IHttpActionResult PostPurpose()
        {
            var ss = crm.T_Roles.Where(p => true).ToList();
            var list = from item in ss
                       select new T_Roles
                       {
                           rId = item.rId,
                           rname = item.rname
                       };
            return Json(list);
        }

        public IHttpActionResult PosteditPurpose(T_Users_Roles ur)
        {
            var ss = crm.T_Users_Roles.SingleOrDefault(p => p.uid == ur.uid);
            System.Guid rid = new Guid("00000000-0000-0000-0000-000000000000");
            if (ur.rId != rid)
            {
                if (ss == null)
                {
                    ur.urid = Guid.NewGuid();
                    crm.T_Users_Roles.Add(ur);
                    var zhi = crm.SaveChanges();
                    if (zhi > 0)
                    {
                        return Json(new { code = true, res = "分配权限成功" });
                    }
                    return Json(new { code = false, res = "分配权限失败" });
                }
                else
                {
                    ss.rId = ur.rId;
                    crm.Entry(ss).State = System.Data.Entity.EntityState.Modified;
                    var zhi = crm.SaveChanges();
                    if (zhi > 0)
                    {
                        return Json(new { code = true, res = "分配权限成功" });
                    }
                    return Json(new { code = false, res = "分配权限失败" });
                }
            }
            return Json(new { code = false, res = "权限不能为空" });
        }

        /// <summary>
        /// Md5转换
        /// </summary>
        /// <param name="inputStr"></param>
        /// <returns></returns>
        public static string MD5(string inputStr)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] hashByte = md5.ComputeHash(Encoding.UTF8.GetBytes(inputStr));
            var sb = new StringBuilder();
            foreach (byte item in hashByte)
                sb.Append(item.ToString("x").PadLeft(2, '0'));
            return sb.ToString();
        }

        public IHttpActionResult Adduser([FromBody] T_Users user)
        {
            user.uid = Guid.NewGuid();
            user.creatime = DateTime.Now;
            user.@lock = "否";
            user.udel = "未删除";
            user.pwderror = 0;
            user.upwd = "e10adc3949ba59abbe56e057f20f883e";
            var ss = crm.T_Users.Where(p => p.uname == user.uname).ToList();
            if (ss.Count() == 0)
            {
                if (user.uname != "")
                {
                    crm.T_Users.Add(user);
                    var zhi = crm.SaveChanges();
                    if (zhi > 0)
                    {
                        return Json(new { code = true, res = "添加员工成功" });
                    }
                    return Json(new { code = false, res = "添加员工失败" });
                }
                return Json(new { code = false, res = "添加员工不能为空" });
            }
            return Json(new { code = false, res = "已经有此角色了" });
        }
    }
}