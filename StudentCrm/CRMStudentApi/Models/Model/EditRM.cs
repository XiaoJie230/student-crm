﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrmStudentApi.Models.Model
{
    public class EditRM
    {
        public string Id { get; set; }
        public string Value { get; set; }
    }
}