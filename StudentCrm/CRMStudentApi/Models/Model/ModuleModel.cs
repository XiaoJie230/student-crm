﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrmStudentApi.Models.Model
{
    public class ModuleModel
    {
        public Guid Moduleid { set; get; }
        public string Modulename { set; get; }
        public Nullable<System.Guid> Parentid { set; get; }
        public string Path { set; get; }
        public int Weight { set; get; }
    }
}