﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrmStudentApi.Models.Model
{
    public class Moduless
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Nullable<System.Guid> ParentId { get; set; }
        public string Path { get; set; }
        public int Weight { get; set; }
        public List<Moduless> children { get; set; }
    }
}