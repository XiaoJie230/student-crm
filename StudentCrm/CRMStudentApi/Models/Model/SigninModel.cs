﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrmStudentApi.Models
{
    public class SigninModel
    {
        public int page { get; set; }
        public int limit { get; set; }
        public string name { get; set; }
        public DateTime sigininTime { get; set; }
        public DateTime siginoutTime { get; set; }
    }
}