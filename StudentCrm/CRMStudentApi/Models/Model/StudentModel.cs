﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrmStudentApi.Models.Model
{
    public class StudentModel
    {
        public int page { get; set; }
        public int limit { get; set; }
        public int Userwlid { get; set; }
        public System.Guid wzid { get; set; }
        public string Stuname { get; set; }
        public string Phone { get; set; }
        public string Education { get; set; }
        public string Fenpei { get; set; }

    }
}