﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrmStudentApi.Models.Model
{
    public class UserModel
    {
        public int page { get; set; }
        public int limit { get; set; }
        public string uname { get; set; }
        public string @lock { get; set; }
    }
}