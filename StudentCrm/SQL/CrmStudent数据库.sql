USE [CrmStudent]
GO
/****** Object:  Table [dbo].[T_Message]    Script Date: 2021/6/20 17:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_Message](
	[mid] [uniqueidentifier] NOT NULL,
	[wid] [uniqueidentifier] NOT NULL,
	[wname] [varchar](50) NOT NULL,
	[zid] [uniqueidentifier] NOT NULL,
	[zname] [varchar](50) NOT NULL,
	[sid] [uniqueidentifier] NOT NULL,
	[sname] [varchar](50) NOT NULL,
	[mtext] [varchar](max) NOT NULL,
	[mtime] [datetime] NOT NULL,
	[state] [varchar](50) NOT NULL,
	[ext1] [int] NOT NULL,
	[ext2] [varchar](50) NOT NULL,
 CONSTRAINT [PK_T_Message] PRIMARY KEY CLUSTERED 
(
	[mid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_Module]    Script Date: 2021/6/20 17:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_Module](
	[mId] [uniqueidentifier] NOT NULL,
	[mname] [varchar](50) NOT NULL,
	[murl] [varchar](50) NULL,
	[mfatherId] [uniqueidentifier] NULL,
	[mweight] [int] NOT NULL,
	[ext1] [int] NULL,
	[ext2] [varchar](50) NULL,
 CONSTRAINT [PK_T_Module] PRIMARY KEY CLUSTERED 
(
	[mId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_Roles]    Script Date: 2021/6/20 17:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_Roles](
	[rId] [uniqueidentifier] NOT NULL,
	[rname] [varchar](50) NOT NULL,
	[ext1] [int] NULL,
	[ext2] [varchar](50) NULL,
 CONSTRAINT [PK_T_Roles] PRIMARY KEY CLUSTERED 
(
	[rId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_Roles_Module]    Script Date: 2021/6/20 17:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_Roles_Module](
	[rmId] [uniqueidentifier] NOT NULL,
	[rId] [uniqueidentifier] NOT NULL,
	[mId] [uniqueidentifier] NOT NULL,
	[ext1] [int] NULL,
	[ext2] [varchar](50) NULL,
 CONSTRAINT [PK_T_Roles_Module] PRIMARY KEY CLUSTERED 
(
	[rmId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_Signin]    Script Date: 2021/6/20 17:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_Signin](
	[sId] [uniqueidentifier] NOT NULL,
	[uId] [uniqueidentifier] NOT NULL,
	[uname] [varchar](50) NULL,
	[signinTime] [datetime] NULL,
	[signoutTime] [datetime] NULL,
	[state] [varchar](50) NOT NULL,
	[ext1] [int] NULL,
	[ext2] [varchar](50) NULL,
 CONSTRAINT [PK_T_Signin] PRIMARY KEY CLUSTERED 
(
	[sId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_Student]    Script Date: 2021/6/20 17:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_Student](
	[Stuid] [uniqueidentifier] NOT NULL,
	[Stuname] [varchar](50) NOT NULL,
	[Sex] [char](2) NOT NULL,
	[Age] [int] NOT NULL,
	[Phone] [varchar](20) NOT NULL,
	[Url] [varchar](50) NOT NULL,
	[Education] [varchar](10) NOT NULL,
	[State] [varchar](10) NOT NULL,
	[Channel] [varchar](50) NULL,
	[Website] [varchar](50) NULL,
	[keywords] [varchar](50) NULL,
	[Attention] [varchar](50) NULL,
	[Department] [varchar](50) NULL,
	[QQ] [varchar](50) NOT NULL,
	[WeChat] [varchar](50) NOT NULL,
	[IsReport] [char](2) NOT NULL,
	[Userwlid] [uniqueidentifier] NOT NULL,
	[Userwlname] [varchar](50) NOT NULL,
	[Userzxid] [uniqueidentifier] NULL,
	[Userzxname] [varchar](50) NULL,
	[Isvalid] [char](2) NULL,
	[Validreason] [varchar](50) NULL,
	[IsReturnvisit] [char](2) NULL,
	[Firstvisittime] [datetime] NULL,
	[IsDoor] [char](2) NULL,
	[Doortime] [datetime] NULL,
	[Deposit] [money] NULL,
	[Deposittime] [datetime] NULL,
	[IsPay] [char](2) NULL,
	[Paytime] [datetime] NULL,
	[Payamount] [money] NULL,
	[IsRefund] [char](2) NULL,
	[RefundReason] [varchar](50) NULL,
	[IsClass] [char](2) NULL,
	[Classtime] [datetime] NULL,
	[Classremarks] [varchar](50) NULL,
	[Consultantremarks] [varchar](50) NULL,
	[ext1] [int] NULL,
	[ext2] [varchar](50) NULL,
 CONSTRAINT [PK_Student] PRIMARY KEY CLUSTERED 
(
	[Stuid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_Users]    Script Date: 2021/6/20 17:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_Users](
	[uid] [uniqueidentifier] NOT NULL,
	[uname] [varchar](50) NOT NULL,
	[upwd] [varchar](50) NOT NULL,
	[uidcard] [varchar](50) NOT NULL,
	[uiPhone] [varchar](11) NOT NULL,
	[uemail] [varchar](50) NOT NULL,
	[creatime] [datetime] NOT NULL,
	[finallytime] [datetime] NULL,
	[pwderror] [int] NULL,
	[lockTime] [datetime] NULL,
	[lock] [varchar](50) NULL,
	[udel] [varchar](50) NULL,
	[ext1] [int] NULL,
	[ext2] [varchar](50) NULL,
 CONSTRAINT [PK_t_users] PRIMARY KEY CLUSTERED 
(
	[uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_Users_Roles]    Script Date: 2021/6/20 17:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_Users_Roles](
	[urid] [uniqueidentifier] NOT NULL,
	[uid] [uniqueidentifier] NOT NULL,
	[rId] [uniqueidentifier] NOT NULL,
	[ext1] [int] NULL,
	[ext2] [varchar](50) NULL,
 CONSTRAINT [PK_T_Users_Roles] PRIMARY KEY CLUSTERED 
(
	[urid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[T_Module] ([mId], [mname], [murl], [mfatherId], [mweight], [ext1], [ext2]) VALUES (N'0a95af7d-e8fa-4da2-b86e-275826783f7d', N'我的学生', N'client/student', N'ad87f769-382f-41c7-9e4f-60fe23b6d6cd', 99, NULL, N'我的学生&&客户系统')
INSERT [dbo].[T_Module] ([mId], [mname], [murl], [mfatherId], [mweight], [ext1], [ext2]) VALUES (N'261b645f-4c44-4b7f-bd7c-3c0527fa7b19', N'网络学生', N'client/netstudent', N'ad87f769-382f-41c7-9e4f-60fe23b6d6cd', 99, NULL, N'网络学生&&客户系统')
INSERT [dbo].[T_Module] ([mId], [mname], [murl], [mfatherId], [mweight], [ext1], [ext2]) VALUES (N'9462a2c2-ef91-4502-9d99-55a0cfab864b', N'角色管理', N'admin/roles', N'b78baba7-a721-4395-b8a2-5bae0b42e1da', 99, NULL, N'角色管理&&系统管理')
INSERT [dbo].[T_Module] ([mId], [mname], [murl], [mfatherId], [mweight], [ext1], [ext2]) VALUES (N'b78baba7-a721-4395-b8a2-5bae0b42e1da', N'系统管理', NULL, NULL, 100, NULL, N'系统管理')
INSERT [dbo].[T_Module] ([mId], [mname], [murl], [mfatherId], [mweight], [ext1], [ext2]) VALUES (N'c80d1fb1-3ff9-4d96-828a-5d9480a329aa', N'签到管理', N'consult/signin', N'e6f026af-03e2-4eff-b5ca-7fedfafe780b', 100, NULL, NULL)
INSERT [dbo].[T_Module] ([mId], [mname], [murl], [mfatherId], [mweight], [ext1], [ext2]) VALUES (N'ad87f769-382f-41c7-9e4f-60fe23b6d6cd', N'客户系统', NULL, NULL, 99, NULL, N'客户系统')
INSERT [dbo].[T_Module] ([mId], [mname], [murl], [mfatherId], [mweight], [ext1], [ext2]) VALUES (N'ed40e24a-08c9-4f26-89bd-79c573b855dd', N'模块管理', N'admin/module', N'b78baba7-a721-4395-b8a2-5bae0b42e1da', 100, NULL, N'模块管理&&系统管理')
INSERT [dbo].[T_Module] ([mId], [mname], [murl], [mfatherId], [mweight], [ext1], [ext2]) VALUES (N'e6f026af-03e2-4eff-b5ca-7fedfafe780b', N'咨询管理', NULL, NULL, 98, NULL, N'咨询管理')
INSERT [dbo].[T_Module] ([mId], [mname], [murl], [mfatherId], [mweight], [ext1], [ext2]) VALUES (N'cf6ab4eb-a9e8-4442-b2fc-ec3004211d69', N'员工管理', N'admin/users', N'b78baba7-a721-4395-b8a2-5bae0b42e1da', 98, NULL, N'员工管理&&系统管理')
INSERT [dbo].[T_Roles] ([rId], [rname], [ext1], [ext2]) VALUES (N'7860b852-a459-4a72-8075-28378695c157', N'管理员', NULL, NULL)
INSERT [dbo].[T_Roles] ([rId], [rname], [ext1], [ext2]) VALUES (N'fcdfb2d4-f12f-486d-a12a-649d237d2af4', N'咨询师', NULL, NULL)
INSERT [dbo].[T_Roles] ([rId], [rname], [ext1], [ext2]) VALUES (N'd0a7a618-b28e-4862-88c1-bcb050c79eb7', N'网络咨询师', NULL, NULL)
INSERT [dbo].[T_Roles] ([rId], [rname], [ext1], [ext2]) VALUES (N'd315cd9b-d426-4046-b004-ef24e46b0c9a', N'咨询经理', NULL, NULL)
INSERT [dbo].[T_Roles_Module] ([rmId], [rId], [mId], [ext1], [ext2]) VALUES (N'ff93387d-bf30-48f5-9bc2-017bc930cb85', N'fcdfb2d4-f12f-486d-a12a-649d237d2af4', N'ad87f769-382f-41c7-9e4f-60fe23b6d6cd', NULL, NULL)
INSERT [dbo].[T_Roles_Module] ([rmId], [rId], [mId], [ext1], [ext2]) VALUES (N'cca4166f-b6cb-4f20-bb4a-097b4840aba8', N'd315cd9b-d426-4046-b004-ef24e46b0c9a', N'0a95af7d-e8fa-4da2-b86e-275826783f7d', NULL, NULL)
INSERT [dbo].[T_Roles_Module] ([rmId], [rId], [mId], [ext1], [ext2]) VALUES (N'3f2b395c-3fdb-40da-94ae-226d19e03f5b', N'7860b852-a459-4a72-8075-28378695c157', N'b78baba7-a721-4395-b8a2-5bae0b42e1da', NULL, N'管理员&&系统管理')
INSERT [dbo].[T_Roles_Module] ([rmId], [rId], [mId], [ext1], [ext2]) VALUES (N'260d7108-8c47-4252-be89-2780ce7f0029', N'd315cd9b-d426-4046-b004-ef24e46b0c9a', N'c80d1fb1-3ff9-4d96-828a-5d9480a329aa', NULL, NULL)
INSERT [dbo].[T_Roles_Module] ([rmId], [rId], [mId], [ext1], [ext2]) VALUES (N'41df10c6-14b8-4c6e-a43e-34e8d8de668b', N'd0a7a618-b28e-4862-88c1-bcb050c79eb7', N'c80d1fb1-3ff9-4d96-828a-5d9480a329aa', NULL, NULL)
INSERT [dbo].[T_Roles_Module] ([rmId], [rId], [mId], [ext1], [ext2]) VALUES (N'4154688b-8324-4b31-a7a9-412bff3b686c', N'7860b852-a459-4a72-8075-28378695c157', N'cf6ab4eb-a9e8-4442-b2fc-ec3004211d69', NULL, N'管理员&&系统管理')
INSERT [dbo].[T_Roles_Module] ([rmId], [rId], [mId], [ext1], [ext2]) VALUES (N'6e90622f-3f9b-4d0c-8555-469d2cb7dbb9', N'd315cd9b-d426-4046-b004-ef24e46b0c9a', N'e6f026af-03e2-4eff-b5ca-7fedfafe780b', NULL, NULL)
INSERT [dbo].[T_Roles_Module] ([rmId], [rId], [mId], [ext1], [ext2]) VALUES (N'3a964cfa-13da-418c-b482-50ae0a7725fa', N'fcdfb2d4-f12f-486d-a12a-649d237d2af4', N'261b645f-4c44-4b7f-bd7c-3c0527fa7b19', NULL, NULL)
INSERT [dbo].[T_Roles_Module] ([rmId], [rId], [mId], [ext1], [ext2]) VALUES (N'7705ee06-a496-4733-9f92-51ed3d35f3b7', N'7860b852-a459-4a72-8075-28378695c157', N'9462a2c2-ef91-4502-9d99-55a0cfab864b', NULL, N'管理员&&角色管理')
INSERT [dbo].[T_Roles_Module] ([rmId], [rId], [mId], [ext1], [ext2]) VALUES (N'bd7dd15b-1748-4a96-8801-59b555167053', N'd315cd9b-d426-4046-b004-ef24e46b0c9a', N'261b645f-4c44-4b7f-bd7c-3c0527fa7b19', NULL, NULL)
INSERT [dbo].[T_Roles_Module] ([rmId], [rId], [mId], [ext1], [ext2]) VALUES (N'69a94c48-9ffc-4592-9e44-636924fcc90c', N'd0a7a618-b28e-4862-88c1-bcb050c79eb7', N'ad87f769-382f-41c7-9e4f-60fe23b6d6cd', NULL, NULL)
INSERT [dbo].[T_Roles_Module] ([rmId], [rId], [mId], [ext1], [ext2]) VALUES (N'ec783f69-91a7-4707-871f-760c7b26e5ab', N'd0a7a618-b28e-4862-88c1-bcb050c79eb7', N'261b645f-4c44-4b7f-bd7c-3c0527fa7b19', NULL, NULL)
INSERT [dbo].[T_Roles_Module] ([rmId], [rId], [mId], [ext1], [ext2]) VALUES (N'9df8cc2a-8322-4d1d-9796-8c1602fde32a', N'd0a7a618-b28e-4862-88c1-bcb050c79eb7', N'0a95af7d-e8fa-4da2-b86e-275826783f7d', NULL, NULL)
INSERT [dbo].[T_Roles_Module] ([rmId], [rId], [mId], [ext1], [ext2]) VALUES (N'7fde55af-9983-4450-92b8-95f8ecde8ac6', N'd315cd9b-d426-4046-b004-ef24e46b0c9a', N'ad87f769-382f-41c7-9e4f-60fe23b6d6cd', NULL, NULL)
INSERT [dbo].[T_Roles_Module] ([rmId], [rId], [mId], [ext1], [ext2]) VALUES (N'2ea5009b-d376-4a48-b4f5-a1ab98a513fc', N'7860b852-a459-4a72-8075-28378695c157', N'ed40e24a-08c9-4f26-89bd-79c573b855dd', NULL, N'管理员&&模块管理')
INSERT [dbo].[T_Roles_Module] ([rmId], [rId], [mId], [ext1], [ext2]) VALUES (N'974ac507-995a-47ab-ae2b-b0cafff1e9c1', N'd0a7a618-b28e-4862-88c1-bcb050c79eb7', N'e6f026af-03e2-4eff-b5ca-7fedfafe780b', NULL, NULL)
INSERT [dbo].[T_Roles_Module] ([rmId], [rId], [mId], [ext1], [ext2]) VALUES (N'2c595e68-516a-478f-9816-cf86c64cf810', N'fcdfb2d4-f12f-486d-a12a-649d237d2af4', N'0a95af7d-e8fa-4da2-b86e-275826783f7d', NULL, NULL)
INSERT [dbo].[T_Signin] ([sId], [uId], [uname], [signinTime], [signoutTime], [state], [ext1], [ext2]) VALUES (N'c85fe372-9f4c-410d-a086-0c15a18966f3', N'8d52c651-1a5c-44a3-b235-b10064d4d09e', N'崔晓杰', CAST(N'2021-06-10T07:59:03.073' AS DateTime), CAST(N'2021-06-10T07:59:03.850' AS DateTime), N'已签退', NULL, NULL)
INSERT [dbo].[T_Signin] ([sId], [uId], [uname], [signinTime], [signoutTime], [state], [ext1], [ext2]) VALUES (N'27a47708-177d-4da4-a567-1c860b238997', N'808850e0-954e-46f0-8ef8-09074c9323b2', N'晓杰', CAST(N'2021-06-16T19:51:33.540' AS DateTime), CAST(N'2021-06-16T19:51:34.153' AS DateTime), N'已签退', NULL, NULL)
INSERT [dbo].[T_Signin] ([sId], [uId], [uname], [signinTime], [signoutTime], [state], [ext1], [ext2]) VALUES (N'2faac365-0cc7-4546-9500-20a8d400500b', N'8d52c651-1a5c-44a3-b235-b10064d4d09e', N'崔晓杰', CAST(N'2021-06-09T11:19:26.707' AS DateTime), CAST(N'2021-06-09T11:19:30.923' AS DateTime), N'已签退', NULL, NULL)
INSERT [dbo].[T_Signin] ([sId], [uId], [uname], [signinTime], [signoutTime], [state], [ext1], [ext2]) VALUES (N'e294cffa-06f4-46b1-a42c-6aae09f538d3', N'a0cbc87b-d2a8-4dc6-9db9-fcc7ccd5e938', N'admin', CAST(N'2021-06-07T14:23:46.617' AS DateTime), CAST(N'2021-06-07T14:25:57.780' AS DateTime), N'已签退', NULL, NULL)
INSERT [dbo].[T_Signin] ([sId], [uId], [uname], [signinTime], [signoutTime], [state], [ext1], [ext2]) VALUES (N'4845eb4d-dfdc-447c-b18e-be44ca40d7a0', N'8d52c651-1a5c-44a3-b235-b10064d4d09e', N'崔晓杰', CAST(N'2021-06-17T15:48:49.033' AS DateTime), CAST(N'2021-06-17T15:48:52.650' AS DateTime), N'已签退', NULL, NULL)
INSERT [dbo].[T_Signin] ([sId], [uId], [uname], [signinTime], [signoutTime], [state], [ext1], [ext2]) VALUES (N'd6c680c3-3217-4312-8932-ca0cb5f3c555', N'8d52c651-1a5c-44a3-b235-b10064d4d09e', N'崔晓杰', CAST(N'2021-06-07T20:23:03.347' AS DateTime), CAST(N'2021-06-07T20:23:12.647' AS DateTime), N'已签退', NULL, NULL)
INSERT [dbo].[T_Signin] ([sId], [uId], [uname], [signinTime], [signoutTime], [state], [ext1], [ext2]) VALUES (N'd829a745-0f4d-4f2e-a0c4-e451370df504', N'a0cbc87b-d2a8-4dc6-9db9-fcc7ccd5e938', N'admin', CAST(N'2021-06-17T16:15:45.773' AS DateTime), CAST(N'2021-06-17T16:15:46.483' AS DateTime), N'已签退', NULL, NULL)
INSERT [dbo].[T_Signin] ([sId], [uId], [uname], [signinTime], [signoutTime], [state], [ext1], [ext2]) VALUES (N'7dbc22e3-b20e-465c-9c4e-ea2c3642deb9', N'8d52c651-1a5c-44a3-b235-b10064d4d09e', N'崔晓杰', CAST(N'2021-06-08T14:09:40.120' AS DateTime), CAST(N'2021-06-08T14:09:43.740' AS DateTime), N'已签退', NULL, NULL)
INSERT [dbo].[T_Signin] ([sId], [uId], [uname], [signinTime], [signoutTime], [state], [ext1], [ext2]) VALUES (N'4da96ea0-ac60-4d24-85a3-efa1a1025aab', N'8d52c651-1a5c-44a3-b235-b10064d4d09e', N'崔晓杰', CAST(N'2021-06-16T17:05:01.873' AS DateTime), CAST(N'2021-06-16T17:05:04.793' AS DateTime), N'已签退', NULL, NULL)
INSERT [dbo].[T_Student] ([Stuid], [Stuname], [Sex], [Age], [Phone], [Url], [Education], [State], [Channel], [Website], [keywords], [Attention], [Department], [QQ], [WeChat], [IsReport], [Userwlid], [Userwlname], [Userzxid], [Userzxname], [Isvalid], [Validreason], [IsReturnvisit], [Firstvisittime], [IsDoor], [Doortime], [Deposit], [Deposittime], [IsPay], [Paytime], [Payamount], [IsRefund], [RefundReason], [IsClass], [Classtime], [Classremarks], [Consultantremarks], [ext1], [ext2]) VALUES (N'00000000-0000-0000-0000-000000000000', N'张', N'男', 18, N'17633544749', N'河南', N'大专', N'未读', N'百度', NULL, NULL, NULL, NULL, N'228113514', N'1626151', N'否', N'8d52c651-1a5c-44a3-b235-b10064d4d09e', N'崔晓杰', N'808850e0-954e-46f0-8ef8-09074c9323b2', N'晓杰', N'是', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'是', NULL, NULL, NULL, NULL, NULL, NULL, N'null')
INSERT [dbo].[T_Student] ([Stuid], [Stuname], [Sex], [Age], [Phone], [Url], [Education], [State], [Channel], [Website], [keywords], [Attention], [Department], [QQ], [WeChat], [IsReport], [Userwlid], [Userwlname], [Userzxid], [Userzxname], [Isvalid], [Validreason], [IsReturnvisit], [Firstvisittime], [IsDoor], [Doortime], [Deposit], [Deposittime], [IsPay], [Paytime], [Payamount], [IsRefund], [RefundReason], [IsClass], [Classtime], [Classremarks], [Consultantremarks], [ext1], [ext2]) VALUES (N'a512a486-15af-451c-bb40-12ccee98c2d6', N'常', N'男', 18, N'17633544749', N'河南', N'大专', N'未读', N'学校', NULL, NULL, NULL, NULL, N'228113516', N'123456', N'是', N'808850e0-954e-46f0-8ef8-09074c9323b2', N'崔晓杰', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'是', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[T_Student] ([Stuid], [Stuname], [Sex], [Age], [Phone], [Url], [Education], [State], [Channel], [Website], [keywords], [Attention], [Department], [QQ], [WeChat], [IsReport], [Userwlid], [Userwlname], [Userzxid], [Userzxname], [Isvalid], [Validreason], [IsReturnvisit], [Firstvisittime], [IsDoor], [Doortime], [Deposit], [Deposittime], [IsPay], [Paytime], [Payamount], [IsRefund], [RefundReason], [IsClass], [Classtime], [Classremarks], [Consultantremarks], [ext1], [ext2]) VALUES (N'2781e6f0-da1a-414e-8c07-65e0dc10232b', N'变', N'男', 23, N'232313212314', N'河南', N'大专', N'未读', N'学校', NULL, NULL, NULL, NULL, N'145454545', N'23132131', N'是', N'439cb697-275e-494f-8367-187ad262e7af', N'杰', N'808850e0-954e-46f0-8ef8-09074c9323b2', N'晓杰', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[T_Student] ([Stuid], [Stuname], [Sex], [Age], [Phone], [Url], [Education], [State], [Channel], [Website], [keywords], [Attention], [Department], [QQ], [WeChat], [IsReport], [Userwlid], [Userwlname], [Userzxid], [Userzxname], [Isvalid], [Validreason], [IsReturnvisit], [Firstvisittime], [IsDoor], [Doortime], [Deposit], [Deposittime], [IsPay], [Paytime], [Payamount], [IsRefund], [RefundReason], [IsClass], [Classtime], [Classremarks], [Consultantremarks], [ext1], [ext2]) VALUES (N'2626ad33-05c6-4d48-87ca-843afcee34fa', N'朱', N'男', 32, N'232124411', N'河南', N'本科', N'未读', N'学校', NULL, NULL, NULL, NULL, N'2312312331', N'213123123', N'是', N'8d52c651-1a5c-44a3-b235-b10064d4d09e', N'崔晓杰', N'8d52c651-1a5c-44a3-b235-b10064d4d09e', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[T_Student] ([Stuid], [Stuname], [Sex], [Age], [Phone], [Url], [Education], [State], [Channel], [Website], [keywords], [Attention], [Department], [QQ], [WeChat], [IsReport], [Userwlid], [Userwlname], [Userzxid], [Userzxname], [Isvalid], [Validreason], [IsReturnvisit], [Firstvisittime], [IsDoor], [Doortime], [Deposit], [Deposittime], [IsPay], [Paytime], [Payamount], [IsRefund], [RefundReason], [IsClass], [Classtime], [Classremarks], [Consultantremarks], [ext1], [ext2]) VALUES (N'd24d0a44-f6ce-4e92-b0e5-acb6d98c9c65', N'崔', N'男', 16, N'17633544749', N'河南', N'大专', N'未读', N'百度', NULL, NULL, NULL, NULL, N'228113514', N'1626151', N'是', N'8d52c651-1a5c-44a3-b235-b10064d4d09e', N'崔晓杰', N'8d52c651-1a5c-44a3-b235-b10064d4d09e', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[T_Users] ([uid], [uname], [upwd], [uidcard], [uiPhone], [uemail], [creatime], [finallytime], [pwderror], [lockTime], [lock], [udel], [ext1], [ext2]) VALUES (N'808850e0-954e-46f0-8ef8-09074c9323b2', N'贾梦波', N'e10adc3949ba59abbe56e057f20f883e', N'412724200002283732', N'17623456789', N'12345678@qq.com', CAST(N'2021-06-04T09:15:36.000' AS DateTime), CAST(N'2021-06-20T17:07:00.140' AS DateTime), 0, NULL, N'否', N'未删除', NULL, NULL)
INSERT [dbo].[T_Users] ([uid], [uname], [upwd], [uidcard], [uiPhone], [uemail], [creatime], [finallytime], [pwderror], [lockTime], [lock], [udel], [ext1], [ext2]) VALUES (N'439cb697-275e-494f-8367-187ad262e7af', N'付育豪', N'e10adc3949ba59abbe56e057f20f883e', N'412724200102743145', N'17698765432', N'87654321@qq.com', CAST(N'2021-06-04T09:15:36.000' AS DateTime), CAST(N'2021-06-17T15:49:38.347' AS DateTime), 0, NULL, N'否', N'未删除', NULL, NULL)
INSERT [dbo].[T_Users] ([uid], [uname], [upwd], [uidcard], [uiPhone], [uemail], [creatime], [finallytime], [pwderror], [lockTime], [lock], [udel], [ext1], [ext2]) VALUES (N'8d52c651-1a5c-44a3-b235-b10064d4d09e', N'常志力', N'e10adc3949ba59abbe56e057f20f883e', N'412724199902283731', N'17687654321', N'776780190@qq.com', CAST(N'2021-06-04T09:15:36.000' AS DateTime), CAST(N'2021-06-20T17:07:23.063' AS DateTime), 0, NULL, N'否', N'未删除', NULL, NULL)
INSERT [dbo].[T_Users] ([uid], [uname], [upwd], [uidcard], [uiPhone], [uemail], [creatime], [finallytime], [pwderror], [lockTime], [lock], [udel], [ext1], [ext2]) VALUES (N'787f61f7-48fa-4c52-a965-f38e9e06ec5f', N'我', N'132', N'3434', N'3434', N'3434', CAST(N'2021-06-04T09:15:36.000' AS DateTime), CAST(N'2021-06-04T09:15:36.000' AS DateTime), 0, NULL, N'否', N'未删除', NULL, NULL)
INSERT [dbo].[T_Users] ([uid], [uname], [upwd], [uidcard], [uiPhone], [uemail], [creatime], [finallytime], [pwderror], [lockTime], [lock], [udel], [ext1], [ext2]) VALUES (N'a0cbc87b-d2a8-4dc6-9db9-fcc7ccd5e938', N'崔晓杰', N'e10adc3949ba59abbe56e057f20f883e', N'410222198706134038', N'17612345678', N'2281135164@qq.com', CAST(N'2021-06-04T09:15:36.000' AS DateTime), CAST(N'2021-06-20T17:12:30.347' AS DateTime), 0, NULL, N'否', N'未删除', NULL, NULL)
INSERT [dbo].[T_Users_Roles] ([urid], [uid], [rId], [ext1], [ext2]) VALUES (N'36372989-013c-4f22-b653-4158246a8a96', N'a0cbc87b-d2a8-4dc6-9db9-fcc7ccd5e938', N'7860b852-a459-4a72-8075-28378695c157', NULL, N'admin&&管理员')
INSERT [dbo].[T_Users_Roles] ([urid], [uid], [rId], [ext1], [ext2]) VALUES (N'd504f49c-6919-48fd-86d3-73b4d66ddb18', N'808850e0-954e-46f0-8ef8-09074c9323b2', N'fcdfb2d4-f12f-486d-a12a-649d237d2af4', NULL, N'晓杰&&咨询师')
INSERT [dbo].[T_Users_Roles] ([urid], [uid], [rId], [ext1], [ext2]) VALUES (N'937fd611-68cd-4a5f-88ea-a74ebdc80bde', N'439cb697-275e-494f-8367-187ad262e7af', N'd0a7a618-b28e-4862-88c1-bcb050c79eb7', NULL, N'杰&&网络咨询师')
INSERT [dbo].[T_Users_Roles] ([urid], [uid], [rId], [ext1], [ext2]) VALUES (N'3b07a0ed-72e3-4d37-8961-bdae3d72d863', N'8d52c651-1a5c-44a3-b235-b10064d4d09e', N'd0a7a618-b28e-4862-88c1-bcb050c79eb7', NULL, N'崔晓杰&&咨询经理')
ALTER TABLE [dbo].[T_Message] ADD  CONSTRAINT [DF_T_Message_mid]  DEFAULT (newid()) FOR [mid]
GO
ALTER TABLE [dbo].[T_Module] ADD  CONSTRAINT [DF_T_Module_mId]  DEFAULT (newid()) FOR [mId]
GO
ALTER TABLE [dbo].[T_Roles] ADD  CONSTRAINT [DF_Table_1_rId]  DEFAULT (newid()) FOR [rId]
GO
ALTER TABLE [dbo].[T_Roles_Module] ADD  CONSTRAINT [DF_T_Roles_Module_rmId]  DEFAULT (newid()) FOR [rmId]
GO
ALTER TABLE [dbo].[T_Signin] ADD  CONSTRAINT [DF_T_Signin_sId]  DEFAULT (newid()) FOR [sId]
GO
ALTER TABLE [dbo].[T_Student] ADD  CONSTRAINT [DF_Student_Stuid]  DEFAULT (newid()) FOR [Stuid]
GO
ALTER TABLE [dbo].[T_Users] ADD  CONSTRAINT [DF_t_users_uid]  DEFAULT (newid()) FOR [uid]
GO
ALTER TABLE [dbo].[T_Users_Roles] ADD  CONSTRAINT [DF_T_Users_Roles_urid]  DEFAULT (newid()) FOR [urid]
GO
ALTER TABLE [dbo].[T_Message]  WITH CHECK ADD  CONSTRAINT [FK_T_Message_T_Student] FOREIGN KEY([sid])
REFERENCES [dbo].[T_Student] ([Stuid])
GO
ALTER TABLE [dbo].[T_Message] CHECK CONSTRAINT [FK_T_Message_T_Student]
GO
ALTER TABLE [dbo].[T_Message]  WITH CHECK ADD  CONSTRAINT [FK_T_Message_T_Users] FOREIGN KEY([wid])
REFERENCES [dbo].[T_Users] ([uid])
GO
ALTER TABLE [dbo].[T_Message] CHECK CONSTRAINT [FK_T_Message_T_Users]
GO
ALTER TABLE [dbo].[T_Message]  WITH CHECK ADD  CONSTRAINT [FK_T_Message_T_Users1] FOREIGN KEY([zid])
REFERENCES [dbo].[T_Users] ([uid])
GO
ALTER TABLE [dbo].[T_Message] CHECK CONSTRAINT [FK_T_Message_T_Users1]
GO
ALTER TABLE [dbo].[T_Roles_Module]  WITH CHECK ADD  CONSTRAINT [FK_T_Roles_Module_T_Module] FOREIGN KEY([mId])
REFERENCES [dbo].[T_Module] ([mId])
GO
ALTER TABLE [dbo].[T_Roles_Module] CHECK CONSTRAINT [FK_T_Roles_Module_T_Module]
GO
ALTER TABLE [dbo].[T_Roles_Module]  WITH CHECK ADD  CONSTRAINT [FK_T_Roles_Module_T_Roles] FOREIGN KEY([rId])
REFERENCES [dbo].[T_Roles] ([rId])
GO
ALTER TABLE [dbo].[T_Roles_Module] CHECK CONSTRAINT [FK_T_Roles_Module_T_Roles]
GO
ALTER TABLE [dbo].[T_Signin]  WITH CHECK ADD  CONSTRAINT [FK_T_Signin_T_Users] FOREIGN KEY([uId])
REFERENCES [dbo].[T_Users] ([uid])
GO
ALTER TABLE [dbo].[T_Signin] CHECK CONSTRAINT [FK_T_Signin_T_Users]
GO
ALTER TABLE [dbo].[T_Student]  WITH CHECK ADD  CONSTRAINT [FK_T_Student_T_Users] FOREIGN KEY([Userwlid])
REFERENCES [dbo].[T_Users] ([uid])
GO
ALTER TABLE [dbo].[T_Student] CHECK CONSTRAINT [FK_T_Student_T_Users]
GO
ALTER TABLE [dbo].[T_Student]  WITH CHECK ADD  CONSTRAINT [FK_T_Student_T_Users1] FOREIGN KEY([Userzxid])
REFERENCES [dbo].[T_Users] ([uid])
GO
ALTER TABLE [dbo].[T_Student] CHECK CONSTRAINT [FK_T_Student_T_Users1]
GO
ALTER TABLE [dbo].[T_Users_Roles]  WITH CHECK ADD  CONSTRAINT [FK_T_Users_Roles_T_Roles] FOREIGN KEY([rId])
REFERENCES [dbo].[T_Roles] ([rId])
GO
ALTER TABLE [dbo].[T_Users_Roles] CHECK CONSTRAINT [FK_T_Users_Roles_T_Roles]
GO
ALTER TABLE [dbo].[T_Users_Roles]  WITH CHECK ADD  CONSTRAINT [FK_T_Users_Roles_T_Users] FOREIGN KEY([uid])
REFERENCES [dbo].[T_Users] ([uid])
GO
ALTER TABLE [dbo].[T_Users_Roles] CHECK CONSTRAINT [FK_T_Users_Roles_T_Users]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'学生stuid和消息sid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_Message', @level2type=N'CONSTRAINT',@level2name=N'FK_T_Message_T_Student'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户uid和消息wid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_Message', @level2type=N'CONSTRAINT',@level2name=N'FK_T_Message_T_Users'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户uid和zid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_Message', @level2type=N'CONSTRAINT',@level2name=N'FK_T_Message_T_Users1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模块mid和角色模块mid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_Roles_Module', @level2type=N'CONSTRAINT',@level2name=N'FK_T_Roles_Module_T_Module'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色rid和角色模块rid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_Roles_Module', @level2type=N'CONSTRAINT',@level2name=N'FK_T_Roles_Module_T_Roles'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户uid和签到uid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_Signin', @level2type=N'CONSTRAINT',@level2name=N'FK_T_Signin_T_Users'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户id和学生userwlid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_Student', @level2type=N'CONSTRAINT',@level2name=N'FK_T_Student_T_Users'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户uid和userzxid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_Student', @level2type=N'CONSTRAINT',@level2name=N'FK_T_Student_T_Users1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色rid和用户角色rid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_Users_Roles', @level2type=N'CONSTRAINT',@level2name=N'FK_T_Users_Roles_T_Roles'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户uid和用户角色表uid' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_Users_Roles', @level2type=N'CONSTRAINT',@level2name=N'FK_T_Users_Roles_T_Users'
GO
