import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/Home.vue'
import Login from '../components/Login.vue'
import Modules from '../components/admin/Module.vue'
import Users from '../components/admin/Users.vue'
import Roles from '../components/admin/Roles.vue'
import netstudent from '../components/client/netstudent.vue'
import student from '../components/client/student.vue'
import track from '../components/client/track.vue'
import signin from '../components/consult/signin.vue'
import weight from '../components/consult/weight.vue'

Vue.use(VueRouter)

const routes = [{
		path: '/',
		redirect: '/Home'
	},
	{
		path: '/Login',
		component: Login
	},
	{
		path: '/Home',
		component: Home,
		children: [{
				path: '/admin/Module',
				component: Modules
			},
			{
				path: '/admin/users',
				component: Users
			},
			{
				path: '/admin/Roles',
				component: Roles
			},
			{
				path: '/client/student',
				component: student
			}, {
				path: '/client/netstudent',
				component: netstudent
			}, {
				path: '/client/track',
				component: track
			},
			{
				path: '/consult/weight',
				component: weight
			},
			{
				path: '/consult/signin',
				component: signin
			}
		]
	}
]

const router = new VueRouter({
	routes
})

router.beforeEach((to, from, next) => {
	//to 将要访问的路径
	//from 代表从哪个路径跳转而来
	//next 是一个函数，表示放行
	//next（） 放行 next（'/login'） 强制跳转

	if (to.path === '/login') return next();
	const tokenStr = window.sessionStorage.getItem('usertoken')
	if (!tokenStr) return next('/login')
	next()
})
export default router
