module.exports = {
	devServer : {
		//前端项目的地址  127.0.0.1 = localhost
		host : "127.0.0.1",
		//端口号
		port : 8080,
		//代理配置
		proxy : {
			//当请求拦截到  /api 的请求
			// /api/login ---> https://localhost:44371/api/login
			'/api' : {
				// 配置跨域请求的域名
				target : "https://localhost:44371/",
				// 是否允许跨域操作
				chageOrigin : true
			}
		}
	}
}